# Copyright (C) 2015  Gilles Degottex <gilles.degottex@gmail.com>

# This file is part of "libqaudioextra".

# "libqaudioextra" is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "libqaudioextra" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

DEFINES += FFT_FFTW3

QT       += gui widgets multimedia

TARGET = qaudioextra
TEMPLATE = lib
CONFIG += staticlib

SOURCES +=  src/qaesigproc.cpp \
            src/qaecolormap.cpp \
            src/qaesettingsauto.cpp

HEADERS +=  include/qaesigproc.h \
            include/qaecolormap.h \
            include/qaesettingsauto.h \
            include/qaeqthelpers.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
