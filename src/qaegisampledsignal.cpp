/*
Copyright (C) 2014  Gilles Degottex <gilles.degottex@gmail.com>

This file is part of "libqaudioextra".

"libqaudioextra" is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"libqaudioextra" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "qaegisampledsignal.h"

#include <limits>
#include <iostream>

#include <qnumeric.h>
#include <QtGlobal>
#include <QPainter>
#include <QGraphicsView>
#include <QScrollBar>
#include <QStyleOptionGraphicsItem>

#include "qaesigproc.h"
#include "qaehelpers.h"

QAEGISampledSignal::QAEGISampledSignal(std::vector<double>* times, std::vector<WAVTYPE>* signal, QGraphicsView* view)
    : m_times(times)
    , m_signal(signal)
    , m_min(-std::numeric_limits<WAVTYPE>::infinity())
    , m_max(std::numeric_limits<WAVTYPE>::infinity())
    , m_mean_fs(-1.0)
    , m_show_zero_values(true)
    , m_gain(1.0)
    , m_shift(0.0)
    , m_view(view)
    , m_markradius(4)
{
    // TODO It should be possible to make a min/max cache as in QAEGIUniformlySampledSignal
    //      and thus avoid this one.
    // TODO Actually cannot use it without managing the scrolling
//    setCacheMode(QGraphicsItem::DeviceCoordinateCache);

    setFlag(QGraphicsItem::ItemUsesExtendedStyleOption, true);

    updateMinMaxValues();
}

void QAEGISampledSignal::setSignal(std::vector<WAVTYPE> *signal) {
    if(m_signal!=signal){
        m_signal = signal;
        updateMinMaxValues();
    }
}

void QAEGISampledSignal::updateMinMaxValues() {
    // TODO Should never be called on long files .... to check in current code
    if(m_signal==NULL || m_signal->empty()){
        m_min = -std::numeric_limits<WAVTYPE>::infinity();
        m_max = std::numeric_limits<WAVTYPE>::infinity();
    }
    else {
        m_min = (*m_signal)[0];
        m_max = (*m_signal)[0];
        WAVTYPE v;
        for(size_t n=1; n<m_signal->size(); ++n) {
            v = (*m_signal)[n];
            m_min = std::min(m_min, v);
            m_max = std::max(m_max, v);
        }
    }

    if(m_signal->size()>1)
        m_mean_fs = 1.0/qae::median(qae::diff(*m_times));
    else
        m_mean_fs = 1.0/0.001; // Def 1ms sampling rate

    prepareGeometryChange();

    //    COUTD << m_min << "," << m_max << std::endl;
}

double QAEGISampledSignal::getMinValue() const {

    return m_gain*m_min+m_shift;
}

double QAEGISampledSignal::getMaxValue() const {

    return m_gain*m_max+m_shift;
}

double QAEGISampledSignal::getMaxAbsoluteValue() const {
    return std::max(std::abs(getMinValue()),std::abs(getMaxValue()));
}

QRectF QAEGISampledSignal::boundingRect() const {
    //    COUTD << "QAEGIUniformSampledSequence::boundingRect " << m_signal << " " << m_min << " " << m_max << std::endl;

    QTransform trans = m_view->transform();

    QRectF rect = m_view->mapToScene(m_view->viewport()->rect()).boundingRect();

    // Fix the abscissa
    if(m_times->size()>0){
        QTransform trans = m_view->transform();
        rect.setLeft(m_times->front() -m_markradius/trans.m11());
        rect.setRight(rect.left()+m_times->back() +2*m_markradius/trans.m11());
    }

    // Fix the ordinate
    rect.setTop(std::min(rect.top(),-getMaxValue()-double(m_markradius/trans.m22())));
    rect.setBottom(std::max(rect.bottom(),-getMinValue()+double(m_markradius/trans.m22())));

//    COUTD << rect << std::endl;
    return rect;
}

void QAEGISampledSignal::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    Q_UNUSED(option)
    Q_UNUSED(widget)

    if(m_times==NULL || m_signal==NULL || m_times->empty())
        return;

    QRectF viewrect = m_view->mapToScene(m_view->viewport()->rect()).boundingRect();
//    DCOUT << "QAEGISampledSignal::paint::paint " << m_signal << " scenerect=" << m_view->sceneRect() << " viewrect=" << viewrect << " update_rect=" << option->exposedRect << std::endl;
    QRectF rect = option->exposedRect;
    double hh = std::abs(m_view->sceneRect().height());
    double yinfmin = -m_view->sceneRect().center().y()-100*hh; // TODO Changes the function ...
    double yinfmax = -m_view->sceneRect().center().y()+100*hh; // TODO Changes the function ...
//    double yinfmin = -100*m_view->sceneRect().bottom(); // TODO Changes the function ...
//    double yinfmax = -100*m_view->sceneRect().top(); // TODO Changes the function ...

    painter->setClipRect(option->exposedRect);

    std::vector<double>::iterator itlb = std::lower_bound(m_times->begin(), m_times->end(), rect.left());
    std::vector<double>::iterator itub = std::upper_bound(m_times->begin(), m_times->end(), rect.right());
//    if(itub==m_times->end())
//        itub = m_times->end()-1;
    int drawfirst = itlb-m_times->begin()-1;
    if(drawfirst<0)
        drawfirst = 0;
    int drawlast = itub-m_times->begin();
    if(drawlast>int(m_times->size())-1)
        drawlast = m_times->size()-1;

    painter->setPen(m_pen);

    if(drawlast>drawfirst){
        //    double f0min = 0.5*gFL->getFs();
        for(int it=drawfirst; it<drawlast; ++it){
    //        if(f0s[ti]>0.0)
    //            f0min = std::min(f0min, f0s[ti]);
            double lv = m_gain*(*m_signal)[it]+m_shift;
            double rv = m_gain*(*m_signal)[it+1]+m_shift;
            if((lv==0 || rv==0) && !m_show_zero_values)
                continue;

//                COUTD << (*m_times)[it] << " " << lf0 << "," << rf0 << std::endl;

            if(!(qIsInf(lv) && qIsInf(rv))){
                // Ensure the values are not creating visual artifacts
                if(lv<yinfmin) lv = yinfmin; // TODO Changes the function ...
                if(lv>yinfmax) lv = yinfmax; // TODO Changes the function ...
                if(rv<yinfmin) rv = yinfmin; // TODO Changes the function ...
                if(rv>yinfmax) rv = yinfmax; // TODO Changes the function ...

//                DCOUT << lv << " " << rv << std::endl;

                painter->drawLine(QLineF((*m_times)[it], -lv, (*m_times)[it+1], -rv));
            }
        }
    }

    // When resolution is big enough, draw tick marks at each sample
    double samppixdensity = viewrect.width()*m_mean_fs/m_view->viewport()->rect().width();
//    COUTD << samppixdensity << std::endl;
    double samppixdensity_dotsthr = 0.1;
    if(samppixdensity<samppixdensity_dotsthr){
        painter->setWorldMatrixEnabled(false); // Work in pixel coordinates

        QRect fullpixrect = m_view->mapFromScene(viewrect).boundingRect();
        double xs2p = (fullpixrect.width()-1)/viewrect.width(); // Scene to pixel
        double ys2p = -(fullpixrect.height()-1)/viewrect.height(); // Scene to pixel
        double xzero = m_view->mapFromScene(QPointF(0.0,0.0)).x();
        double yzero = m_view->mapFromScene(QPointF(0.0,0.0)).y();

        qreal markradius = m_markradius;
        markradius *= (samppixdensity_dotsthr-samppixdensity)/samppixdensity_dotsthr;

        QPen pointpen(m_pen.color());
        pointpen.setCosmetic(true);
        pointpen.setWidth(2*markradius+1);
        pointpen.setCapStyle(Qt::RoundCap);
        painter->setPen(pointpen);

        for(int it=drawfirst; it<=drawlast; ++it){

            double t = (*m_times)[it];
            WAVTYPE v = m_gain*(*m_signal)[it]+m_shift;

            if(v==0.0 && !m_show_zero_values)
                continue;

            if(!qIsInf(v) && v>yinfmin && v<yinfmax)
                painter->drawPoint(QPointF(t*xs2p+xzero,v*ys2p+yzero));
        }

        painter->setWorldMatrixEnabled(true); // Work in pixel coordinates
    }
}
