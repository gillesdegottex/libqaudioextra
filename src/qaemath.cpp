#ifndef QAEMATH_H
#define QAEMATH_H

//#include "../include/qaemath.h"
#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>
#include <complex>
#include <iostream>

namespace qae {

#define TLTRIGT double

int tltrig_N = -1;
TLTRIGT tltrig_v2n;
std::vector<TLTRIGT> tltrig_cos;
std::vector<TLTRIGT> tltrig_sin; // TODO Use tltrig_cos
std::vector<std::complex<TLTRIGT> > tltrig_expi; // TODO Use tltrig_cos

void tltrig_init(int N){
    if(N!=tltrig_N && N!=-1){
        tltrig_cos.resize(N);
        tltrig_sin.resize(N);
        tltrig_expi.resize(N);

        for(size_t n=0; n<tltrig_cos.size(); ++n)
            tltrig_cos[n] = std::cos(2.0*M_PI*n/N);

        for(size_t n=0; n<tltrig_sin.size(); ++n)
            tltrig_sin[n] = std::sin(2.0*M_PI*n/N);

        for(size_t n=0; n<tltrig_expi.size(); ++n)
            tltrig_expi[n] = std::complex<TLTRIGT>(tltrig_cos[n], tltrig_sin[n]);

        tltrig_N = N;
        tltrig_v2n = N/(2.0*M_PI);
    }
}

};

#endif // QAEMATH_H
