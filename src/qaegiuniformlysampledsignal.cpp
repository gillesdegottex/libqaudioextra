/*
Copyright (C) 2014  Gilles Degottex <gilles.degottex@gmail.com>

This file is part of "libqaudioextra".

"libqaudioextra" is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"libqaudioextra" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "qaegiuniformlysampledsignal.h"

#include <limits>
#include <iostream>

#include <qnumeric.h>
#include <QtGlobal>
#include <QPainter>
#include <QGraphicsView>
#include <QScrollBar>
#include <QStyleOptionGraphicsItem>

#include "qaesigproc.h"
#include "qaehelpers.h"

bool QAEGIUniformlySampledSignal::PaintParameters::operator==(const PaintParameters& param) const {
    if(m_winpixdelay!=param.m_winpixdelay)
        return false;
    if(m_seq!=param.m_seq)
        return false;
    if(m_fullpixrect!=param.m_fullpixrect)
        return false;
    if(m_viewrect!=param.m_viewrect)
        return false;

    return true;
}

QAEGIUniformlySampledSignal::QAEGIUniformlySampledSignal(std::vector<WAVTYPE>* seq, double fs, QGraphicsView* view)
    : m_seq(seq)
    , m_min(-std::numeric_limits<WAVTYPE>::infinity())
    , m_max(std::numeric_limits<WAVTYPE>::infinity())
    , m_fs(fs)
    , m_delay(0)
    , m_gain(1.0)
    , m_clip_min(-std::numeric_limits<WAVTYPE>::infinity())
    , m_clip_max(std::numeric_limits<WAVTYPE>::infinity())
    , m_view(view)
    , m_markradius(4)
{
    // Using Qt's cache doesn't make sens since the cache of a 1D Sequence needs
    // only two values (min and max) for each pixel, not a full image.
    // setCacheMode(QGraphicsItem::DeviceCoordinateCache);

    setFlag(QGraphicsItem::ItemUsesExtendedStyleOption, true); // For exposedRect

    updateMinMaxValues();
}

void QAEGIUniformlySampledSignal::setSignal(std::vector<WAVTYPE> *seq) {
    if(m_seq!=seq){
        m_seq = seq;
        updateMinMaxValues();
        clearCache();
    }
}

void QAEGIUniformlySampledSignal::setSamplingRate(double fs) {
    if(m_fs!=fs){
        m_fs = fs;
        prepareGeometryChange();
        clearCache();
    }
}

void QAEGIUniformlySampledSignal::setGain(double gain) {
    if(m_gain!=gain){
        m_gain = gain;
        prepareGeometryChange();
        clearCache();
    }
}

void QAEGIUniformlySampledSignal::setDelay(qint64 delay) {
    if(m_delay!=delay){
        m_delay = delay;
        prepareGeometryChange();
        clearCache();
    }
}

void QAEGIUniformlySampledSignal::setClip(double min, double max) {
    if(m_clip_min!=min || m_clip_max!=max){
        m_clip_min = min;
        m_clip_max = max;
        prepareGeometryChange();
        clearCache();
    }
}

void QAEGIUniformlySampledSignal::updateMinMaxValues() {
    // TODO Should never be called on long files .... to check in current code
    if(m_seq==NULL || m_seq->empty()){
        m_min = -std::numeric_limits<WAVTYPE>::infinity();
        m_max = std::numeric_limits<WAVTYPE>::infinity();
    }
    else {
        m_min = (*m_seq)[0];
        m_max = (*m_seq)[0];
        WAVTYPE v;
        for(size_t n=1; n<m_seq->size(); ++n) {
            v = (*m_seq)[n];
            m_min = std::min(m_min, v);
            m_max = std::max(m_max, v);
        }
    }

    prepareGeometryChange();

    //    COUTD << m_min << "," << m_max << std::endl;
}

double QAEGIUniformlySampledSignal::getMinValue() const {

    if(m_gain*m_min<m_clip_min)
        return m_clip_min;

    return m_gain*m_min;
}

double QAEGIUniformlySampledSignal::getMaxValue() const {

    if(m_gain*m_max>m_clip_max)
        return m_clip_max;

    return m_gain*m_max;
}

double QAEGIUniformlySampledSignal::getMaxAbsoluteValue() const {
    return std::abs(m_gain*std::max(std::abs(std::max(m_min,m_clip_min)),std::abs(std::min(m_max,m_clip_max))));
}

QRectF QAEGIUniformlySampledSignal::boundingRect() const {
    //    COUTD << "QAEGIUniformSampledSequence::boundingRect " << m_seq << " " << m_min << " " << m_max << std::endl;

    QTransform trans = m_view->transform();

    QRectF rect = m_view->mapToScene(m_view->viewport()->rect()).boundingRect();

    // Fix the abscissa
    rect.setLeft(m_delay/m_fs - double(m_markradius/trans.m11()));
    rect.setRight((qint64(m_seq->size())+m_delay)/m_fs + double(m_markradius/trans.m11()));

    // Fix the ordinate
    rect.setTop(std::min(rect.top(),-getMaxValue()-double(m_markradius/trans.m22())));
    rect.setBottom(std::max(rect.bottom(),-getMinValue()+double(m_markradius/trans.m22())));

    double inftop = -1e6;
    double infbottom = 1e6;
    if(m_view){
        inftop = 1000*m_view->mapToScene(m_view->viewport()->rect()).boundingRect().top();
        infbottom = 1000*m_view->mapToScene(m_view->viewport()->rect()).boundingRect().bottom();
    }
    if(qIsInf(rect.top())) rect.setTop(inftop);
    if(qIsInf(rect.bottom())) rect.setBottom(infbottom);

    return rect;
}

void QAEGIUniformlySampledSignal::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    Q_UNUSED(widget)
//    COUTD << "FTSound::GraphicItemWaveform::paint " << m_seq << " update_rect=" << option->exposedRect << std::endl;

    if(m_seq==NULL || m_seq->empty())
        return;

    painter->setClipRect(option->exposedRect);

    QRectF viewrect = m_view->mapToScene(m_view->viewport()->rect()).boundingRect();
//    double yinfmin = -std::numeric_limits<WAVTYPE>::max(); // This one creates visual artifacts
//    double yinfmax = std::numeric_limits<WAVTYPE>::max();
    double hh = std::abs(m_view->sceneRect().height());
    double yinfmin = -m_view->sceneRect().center().y()-100*hh; // TODO Changes the function ...
    double yinfmax = -m_view->sceneRect().center().y()+100*hh; // TODO Changes the function ...
//    double yinfmin = -100*m_view->sceneRect().bottom(); // TODO Changes the function ...
//    double yinfmax = -100*m_view->sceneRect().top(); // TODO Changes the function ...
//    COUTD << "yinfmin=" << yinfmin << " yinfmax=" << yinfmax << std::endl;
//    COUTD << "viewrect=" << viewrect << endl;
    QRectF rect = option->exposedRect;
//    COUTD << "rect to update=" << rect << endl;

    // TODO Compute this the other way, so that it expresses the number of pixels per sample
    double samppixdensity = (viewrect.right()-viewrect.left())*m_fs/m_view->viewport()->rect().width();
//    COUTD << samppixdensity << std::endl;

//    samppixdensity=5;
    if(samppixdensity<4) {
//        COUTD << "Draw lines between each sample" << std::endl;

        WAVTYPE gain = m_gain;

        painter->setPen(m_pen);

        qint64 nleft = int((rect.left())*m_fs)-m_delay;
        qint64 nright = int((rect.right())*m_fs)+1-m_delay;
        nleft = std::max(nleft, -m_delay);
        nleft = std::max(nleft, qint64(0));
        nleft = std::min(nleft, qint64(m_seq->size()-1)-m_delay);
        nleft = std::min(nleft, qint64(m_seq->size()-1));
        nright = std::max(nright, -m_delay);
        nright = std::max(nright, qint64(0));
        nright = std::min(nright, qint64(m_seq->size()-1)-m_delay);
        nright = std::min(nright, qint64(m_seq->size()-1));

        // Draw a line between each sample value
        WAVTYPE dt = 1.0/m_fs;
        WAVTYPE prevx = (nleft+m_delay)*dt;
        WAVTYPE* data = m_seq->data();
        WAVTYPE prevy = *(data+nleft);
        prevy *= gain;
        if(prevy>m_clip_max)      prevy = m_clip_max;
        else if(prevy<m_clip_min) prevy = m_clip_min;

        WAVTYPE x, y, yy, prevyy;
        for(int n=nleft; n<=nright; ++n){
            x = (n+m_delay)*dt;
            y = *(data+n);

            // Apply gain and clipping
            y *= gain;
            if(y>m_clip_max)      y = m_clip_max;
            else if(y<m_clip_min) y = m_clip_min;

            if(!(qIsInf(prevy) && qIsInf(y))){
                yy = y;
                // Ensure the value is not creating visual artifacts
                if(yy<yinfmin) yy = yinfmin; // TODO Changes the function ...
                if(yy>yinfmax) yy = yinfmax; // TODO Changes the function ...
                prevyy = prevy;
                if(prevyy<yinfmin) prevyy = yinfmin; // TODO Changes the function ...
                if(prevyy>yinfmax) prevyy = yinfmax; // TODO Changes the function ...

                painter->drawLine(QLineF(prevx, -prevyy, x, -yy));
            }
//            std::cout << x << "," << yy << std::endl;

            prevx = x;
            prevy = y;
        }

        // When resolution is big enough, draw tick marks at each sample
        double samppixdensity_dotsthr = 0.1;
        if(samppixdensity<samppixdensity_dotsthr){
            painter->setWorldMatrixEnabled(false); // Work in pixel coordinates
            QRect fullpixrect = m_view->mapFromScene(viewrect).boundingRect();
            double xs2p = (fullpixrect.width()-1)/viewrect.width(); // Scene to pixel
            double ys2p = -(fullpixrect.height()-1)/viewrect.height(); // Scene to pixel
            double xzero = m_view->mapFromScene(QPointF(0.0,0.0)).x();
            double yzero = m_view->mapFromScene(QPointF(0.0,0.0)).y();

            qreal markradius = m_markradius;
            markradius *= (samppixdensity_dotsthr-samppixdensity)/samppixdensity_dotsthr;

            QPen pointpen(m_pen.color());
            pointpen.setCosmetic(true);
            pointpen.setWidth(2*markradius+1);
            pointpen.setCapStyle(Qt::RoundCap);
            painter->setPen(pointpen);

            for(int n=nleft; n<=nright; n++){
                x = (n+m_delay)*dt;

                y = (*(data+n));

                y *= gain;
                if(y>m_clip_max)      y = m_clip_max;
                else if(y<m_clip_min) y = m_clip_min;

                if(!qIsInf(y) && y>yinfmin && y<yinfmax)
                    painter->drawPoint(QPointF(x*xs2p+xzero,y*ys2p+yzero));
            }

            painter->setWorldMatrixEnabled(true); // Work in pixel coordinates
        }
    }
    else {
//        COUTD << "Plot only one line per pixel" << std::endl;

        painter->setWorldMatrixEnabled(false); // Work in pixel coordinates

        double gain = m_gain;

        painter->setPen(m_pen);

        QRect fullpixrect = m_view->mapFromScene(viewrect).boundingRect();
        QRect pixrect = m_view->mapFromScene(rect).boundingRect();
        pixrect = pixrect.intersected(fullpixrect);

        double s2p = -(fullpixrect.height()-1)/viewrect.height(); // Scene to pixel
        double p2n = m_fs*double(viewrect.width())/double(fullpixrect.width()-1); // Pixel to scene
        double yzero = m_view->mapFromScene(QPointF(0.0,0.0)).y();

        WAVTYPE* yp = m_seq->data();

        int winpixdelay = m_view->horizontalScrollBar()->value(); // - 1.0/p2n; // The magic value to make everything plot at the same place whatever the scroll

        int snddelay = m_delay;

        PaintParameters reqparams(fullpixrect, viewrect, winpixdelay, m_seq);
        if(reqparams==m_paint_params){
//            DCOUT << "Using existing buffer " << pixrect << std::endl;
            for(int i=pixrect.left(); i<=pixrect.right(); i++){
//            for(int i=0; i<=snd->m_wavpx_min.size(); i++)
                // If the line is visible, print it
                if(m_cache_mins[i]<std::numeric_limits<WAVTYPE>::infinity() && m_cache_maxs[i]<std::numeric_limits<WAVTYPE>::infinity())
                    painter->drawLine(QLineF(i, m_cache_mins[i], i, m_cache_maxs[i]));
            }
        }
        else {
//            DCOUT << "Re compute buffer and draw " << pixrect << std::endl;
            if(int(m_cache_mins.size())!=reqparams.m_fullpixrect.width()){
                m_cache_mins.resize(reqparams.m_fullpixrect.width());
                m_cache_maxs.resize(reqparams.m_fullpixrect.width());
                pixrect = reqparams.m_fullpixrect; // Redraw the full width of the view
            }
            int ns = int((pixrect.left()+winpixdelay)*p2n)-snddelay;
            for(int i=pixrect.left(); i<=pixrect.right(); i++) {

                int ne = int((i+1+winpixdelay)*p2n)-snddelay;

                if(ns<0 || ne>=int(m_seq->size())) {
                    // If sample is out of the signal, put it at +Inf
                    m_cache_mins[i] = std::numeric_limits<WAVTYPE>::infinity();
                    m_cache_maxs[i] = std::numeric_limits<WAVTYPE>::infinity();
                }
                else {
                    WAVTYPE ymin = std::numeric_limits<WAVTYPE>::infinity();
                    WAVTYPE ymax = -std::numeric_limits<WAVTYPE>::infinity();
                    WAVTYPE* ypp = yp+ns;
                    WAVTYPE y;
                    for(int n=ns; n<=ne; ++n) {
                        y = *ypp;
                        ymin = std::min(ymin, y);
                        ymax = std::max(ymax, y);
                        ypp++;
                    }

                    // Apply gain and clipping
                    ymin = gain*ymin;
                    if(ymin>m_clip_max)         ymin = m_clip_max;
                    else if(ymin<m_clip_min)    ymin = m_clip_min;
                    ymax = gain*ymax;
                    if(ymax>m_clip_max)         ymax = m_clip_max;
                    else if(ymax<m_clip_min)    ymax = m_clip_min;

                    // Ensure the value is not creating visual artifacts
                    if(ymin<yinfmin) ymin = yinfmin;// TODO Changes the function ...
                    if(ymin>yinfmax) ymin = yinfmax;// TODO Changes the function ...
                    if(ymax<yinfmin) ymax = yinfmin;// TODO Changes the function ...
                    if(ymax>yinfmax) ymax = yinfmax;// TODO Changes the function ...

                    // The following seems to reproduce the Qt drawing in scene coordinates
                    ymin *= s2p;
                    ymax *= s2p;
                    ymin += yzero;
                    ymax += yzero;
                    ymin = int(ymin-0.5);
                    ymax = int(ymax+0.5);
//                    std::cout << ymin << "," << ymax << std::endl;
                    m_cache_mins[i] = ymin;
                    m_cache_maxs[i] = ymax;
                    painter->drawLine(QLineF(i, ymin, i, ymax));
                }

                ns = ne;
            }
        }
        m_paint_params = reqparams;

        painter->setWorldMatrixEnabled(true); // Go back to scene coordinates
    }
}
