/*
Copyright (C) 2007  Gilles Degottex <gilles.degottex@gmail.com>

This file is part of "libqaudioextra".

"libqaudioextra" is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"libqaudioextra" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
using namespace std;
#include "../include/qaesettingsauto.h"

#include <qcheckbox.h>
#include <qspinbox.h>
#include <qlineedit.h>
#include <qcombobox.h>
#include <qgroupbox.h>
#include <qradiobutton.h>
#include <qaction.h>
#include <qlabel.h>

QAESettingsAuto::QAESettingsAuto(const QString& organization, const QString& application)
    : QSettings(QSettings::UserScope, organization, application)
{
//	beginGroup(QString("/")+product+setting_version+"/");
    cout << "INFO: QAESettingsAuto: " << fileName().toStdString() << " " << allKeys().size() << " entries" << endl;
}

QAESettingsAuto::QAESettingsAuto()
{
//    beginGroup(QString("/")+product+setting_version+"/");
    cout << "INFO: QAESettingsAuto: " << fileName().toStdString() << " " << allKeys().size() << " entries" << endl;
}

void QAESettingsAuto::add(QCheckBox* el)
{
	assert(el->objectName()!="");
	m_elements_checkbox.push_back(el);
    if(contains(el->objectName()))
        load(el);
}
void QAESettingsAuto::save(QCheckBox* el)
{
    beginGroup("QAESettingsAuto/");
    setValue(el->objectName(), el->isChecked());
    endGroup();
}
void QAESettingsAuto::load(QCheckBox* el)
{
    beginGroup("QAESettingsAuto/");
    el->setChecked(value(el->objectName(), el->isChecked()).toBool());
    endGroup();
}

void QAESettingsAuto::add(QSpinBox* el)
{
	assert(el->objectName()!="");
	m_elements_spinbox.push_back(el);
    if(contains(el->objectName()))
        load(el);
}
void QAESettingsAuto::save(QSpinBox* el)
{
    beginGroup("QAESettingsAuto/");
    setValue(el->objectName(), el->value());
    endGroup();
}
void QAESettingsAuto::load(QSpinBox* el)
{
    beginGroup("QAESettingsAuto/");
    el->setValue(value(el->objectName(), el->value()).toInt());
    endGroup();
}

void QAESettingsAuto::add(QDoubleSpinBox* el)
{
    assert(el->objectName()!="");
    m_elements_doublespinbox.push_back(el);
    if(contains(el->objectName()))
        load(el);
}
void QAESettingsAuto::save(QDoubleSpinBox* el)
{
    beginGroup("QAESettingsAuto/");
    setValue(el->objectName(), el->value());
    endGroup();
}
void QAESettingsAuto::load(QDoubleSpinBox* el)
{
    beginGroup("QAESettingsAuto/");
    el->setValue(value(el->objectName(), el->value()).toDouble());
    endGroup();
}

void QAESettingsAuto::add(QLineEdit* el)
{
    assert(el->objectName()!="");
    m_elements_lineedit.push_back(el);
    if(contains(el->objectName()))
        load(el);
}
void QAESettingsAuto::save(QLineEdit* el)
{
    beginGroup("QAESettingsAuto/");
    setValue(el->objectName(), el->text());
    endGroup();
}
void QAESettingsAuto::load(QLineEdit* el)
{
    beginGroup("QAESettingsAuto/");
    el->setText(value(el->objectName(), (el->text())).toString());
    endGroup();
}

void QAESettingsAuto::add(QComboBox* el, bool usetext)
{
    assert(el->objectName()!="");
    m_elements_combobox.push_back(el);
    m_elements_combobox_usetext[el] = usetext;
    if(contains(el->objectName()))
        load(el);
}
void QAESettingsAuto::save(QComboBox* el)
{
    beginGroup("QAESettingsAuto/");
    std::map<QComboBox*,bool>::iterator it=m_elements_combobox_usetext.find(el);
    if(it!=m_elements_combobox_usetext.end() && it->second)
        setValue(el->objectName(), el->currentText());
    else
        setValue(el->objectName(), el->currentIndex());
    endGroup();
}
void QAESettingsAuto::load(QComboBox* el)
{
    beginGroup("QAESettingsAuto/");
    std::map<QComboBox*,bool>::iterator it=m_elements_combobox_usetext.find(el);
    if(it!=m_elements_combobox_usetext.end() && it->second)
        el->setCurrentText(value(el->objectName(), el->currentText()).toString());
    else
        el->setCurrentIndex(value(el->objectName(), el->currentIndex()).toInt());
    endGroup();
}

void QAESettingsAuto::add(QGroupBox* el)
{
    assert(el->objectName()!="");
    m_elements_qgroupbox.push_back(el);
    if(contains(el->objectName()))
        load(el);
}
void QAESettingsAuto::save(QGroupBox* el)
{
    beginGroup("QAESettingsAuto/");
    setValue(el->objectName(), el->isChecked());
    endGroup();
}
void QAESettingsAuto::load(QGroupBox* el)
{
    beginGroup("QAESettingsAuto/");
    el->setChecked(value(el->objectName(), el->isChecked()).toBool());
    endGroup();
}

void QAESettingsAuto::add(QRadioButton* el)
{
    assert(el->objectName()!="");
    m_elements_qradiobutton.push_back(el);
    if(contains(el->objectName()))
        load(el);
}
void QAESettingsAuto::save(QRadioButton* el)
{
    beginGroup("QAESettingsAuto/");
    setValue(el->objectName(), el->isChecked());
    endGroup();
}
void QAESettingsAuto::load(QRadioButton* el)
{
    beginGroup("QAESettingsAuto/");
    el->setChecked(value(el->objectName(), el->isChecked()).toBool());
    endGroup();
}

void QAESettingsAuto::add(QSlider* el)
{
    assert(el->objectName()!="");
    m_elements_qslider.push_back(el);
    if(contains(el->objectName()))
        load(el);
}
void QAESettingsAuto::save(QSlider* el)
{
    beginGroup("QAESettingsAuto/");
    setValue(el->objectName(), el->value());
    endGroup();
}
void QAESettingsAuto::load(QSlider* el)
{
    beginGroup("QAESettingsAuto/");
    el->setValue(value(el->objectName(), el->value()).toInt());
    endGroup();
}

void QAESettingsAuto::add(QAction* el)
{
    assert(el->objectName()!="");
    m_elements_qaction.push_back(el);
    if(contains(el->objectName()))
        load(el);
}
void QAESettingsAuto::save(QAction* el)
{
    beginGroup("QAESettingsAuto/");
    setValue(el->objectName(), el->isChecked());
    endGroup();
}
void QAESettingsAuto::load(QAction* el)
{
    beginGroup("QAESettingsAuto/");
    el->setChecked(value(el->objectName(), el->isChecked()).toBool());
    endGroup();
}

void QAESettingsAuto::addFont(QLabel* el)
{
    assert(el->objectName()!="");
    m_elements_qfont.push_back(el);
    if(contains(el->objectName()+"_family"))
        loadFont(el);
}
void QAESettingsAuto::saveFont(QLabel* el)
{
    beginGroup("QAESettingsAuto/");
    setValue(el->objectName()+"_family", (*el).font().family());
    setValue(el->objectName()+"_pointSize", (*el).font().pointSize());
    endGroup();
}
void QAESettingsAuto::loadFont(QLabel* el)
{
    QFont f = el->font();
    beginGroup("QAESettingsAuto/");
    f.setFamily(value(el->objectName()+"_family", f.family()).toString());
    f.setPointSize(value(el->objectName()+"_pointSize", f.pointSize()).toInt());
    el->setFont(f);
    endGroup();
}

bool QAESettingsAuto::contains(const QString & key) {
    bool ret;

    beginGroup("QAESettingsAuto/");
    ret = QSettings::contains(key);
    endGroup();

    return ret;
}

void QAESettingsAuto::saveAll()
{
	for(list<QCheckBox*>::iterator it=m_elements_checkbox.begin(); it!=m_elements_checkbox.end(); it++)
        save(*it);

	for(list<QSpinBox*>::iterator it=m_elements_spinbox.begin(); it!=m_elements_spinbox.end(); it++)
        save(*it);

    for(list<QDoubleSpinBox*>::iterator it=m_elements_doublespinbox.begin(); it!=m_elements_doublespinbox.end(); it++){
        save(*it);
    }

	for(list<QLineEdit*>::iterator it=m_elements_lineedit.begin(); it!=m_elements_lineedit.end(); it++)
        save(*it);

	for(list<QComboBox*>::iterator it=m_elements_combobox.begin(); it!=m_elements_combobox.end(); it++)
        save(*it);

	for(list<QGroupBox*>::iterator it=m_elements_qgroupbox.begin(); it!=m_elements_qgroupbox.end(); it++)
        save(*it);

	for(list<QRadioButton*>::iterator it=m_elements_qradiobutton.begin(); it!=m_elements_qradiobutton.end(); it++)
        save(*it);

    for(list<QSlider*>::iterator it=m_elements_qslider.begin(); it!=m_elements_qslider.end(); it++)
        save(*it);

    for(list<QAction*>::iterator it=m_elements_qaction.begin(); it!=m_elements_qaction.end(); it++)
        save(*it);

    for(list<QLabel*>::iterator it=m_elements_qfont.begin(); it!=m_elements_qfont.end(); it++)
        saveFont(*it);

    sync();
}
void QAESettingsAuto::loadAll()
{
	for(list<QCheckBox*>::iterator it=m_elements_checkbox.begin(); it!=m_elements_checkbox.end(); it++)
        load(*it);

	for(list<QSpinBox*>::iterator it=m_elements_spinbox.begin(); it!=m_elements_spinbox.end(); it++)
        load(*it);

    for(list<QDoubleSpinBox*>::iterator it=m_elements_doublespinbox.begin(); it!=m_elements_doublespinbox.end(); it++){
        load(*it);
    }

	for(list<QLineEdit*>::iterator it=m_elements_lineedit.begin(); it!=m_elements_lineedit.end(); it++)
        load(*it);

	for(list<QComboBox*>::iterator it=m_elements_combobox.begin(); it!=m_elements_combobox.end(); it++)
        load(*it);

	for(list<QGroupBox*>::iterator it=m_elements_qgroupbox.begin(); it!=m_elements_qgroupbox.end(); it++)
        load(*it);

	for(list<QRadioButton*>::iterator it=m_elements_qradiobutton.begin(); it!=m_elements_qradiobutton.end(); it++)
        load(*it);

    for(list<QSlider*>::iterator it=m_elements_qslider.begin(); it!=m_elements_qslider.end(); it++)
        load(*it);

    for(list<QAction*>::iterator it=m_elements_qaction.begin(); it!=m_elements_qaction.end(); it++)
        load(*it);

    for(list<QLabel*>::iterator it=m_elements_qfont.begin(); it!=m_elements_qfont.end(); it++)
        loadFont(*it);

    sync();
}
void QAESettingsAuto::clearAll()
{
    QAESettingsAuto::clear();

    sync();
}


//    QStringList	l = allKeys();
//    for(int f=0; f<l.size(); f++){
//        cout << f << ": " << l.at(f).toLatin1().constData() << endl;
//    }
