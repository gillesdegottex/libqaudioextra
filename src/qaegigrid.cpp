/*
Copyright (C) 2015  Gilles Degottex <gilles.degottex@gmail.com>

This file is part of "libqaudioextra".

"libqaudioextra" is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"libqaudioextra" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "qaegigrid.h"

#include <QPen>
#include <QGraphicsView>

#include <cmath>
#include <iostream>

#include "qaehelpers.h"


QAEGIGrid::QAEGIGrid(QGraphicsView* view, const QString &xunit, const QString &yunit)
    : m_qfm_labels(m_font)
{
    m_view = view;

    m_xunit = xunit;
    m_yunit = yunit;

    m_highcontrast = false;
    m_mathyaxis = false;

    // Prepare the pens and fonts
    m_pen_hc_back = QPen(Qt::black);
    m_pen_hc_back.setWidth(0); // Cosmetic pen
    m_pen_hc_front = QPen(Qt::white);
    m_pen_hc_front.setWidth(0); // Cosmetic pen
    m_pen_lines = QPen(QColor(192,192,192));
    m_pen_lines.setWidth(0); // Cosmetic pen
    m_pen_labels = QPen(QColor(128,128,128));
    m_pen_labels.setWidth(0); // Cosmetic pen
}

void QAEGIGrid::setFont(const QFont &font){
    m_font = font;
    m_font_hc_back = m_font;
    m_font_hc_back.setBold(true);
    m_qfm_labels = QFontMetrics(m_font);

    for(size_t n=0; n<m_xlabelsback.size(); ++n)
        m_xlabelsback[n]->setFont(font);
    for(size_t n=0; n<m_xlabels.size(); ++n)
        m_xlabels[n]->setFont(font);
    for(size_t n=0; n<m_ylabelsback.size(); ++n)
        m_ylabelsback[n]->setFont(font);
    for(size_t n=0; n<m_ylabels.size(); ++n)
        m_ylabels[n]->setFont(font);
}

void QAEGIGrid::setHighContrast(bool highcontrast){
    m_highcontrast = highcontrast;

    // TODO Change pens for managing dynamic changes of m_highcontrast
}

QAEGIGrid::~QAEGIGrid(){
    while(!m_vertlinesback.empty()){
        delete m_vertlinesback.back();
        m_vertlinesback.pop_back();
    }
    while(!m_horilinesback.empty()){
        delete m_horilinesback.back();
        m_horilinesback.pop_back();
    }
    while(!m_xlabelsback.empty()){
        delete m_xlabelsback.back();
        m_xlabelsback.pop_back();
    }
    while(!m_ylabelsback.empty()){
        delete m_ylabelsback.back();
        m_ylabelsback.pop_back();
    }

    while(!m_vertlines.empty()){
        delete m_vertlines.back();
        m_vertlines.pop_back();
    }
    while(!m_horilines.empty()){
        delete m_horilines.back();
        m_horilines.pop_back();
    }
    while(!m_xlabels.empty()){
        delete m_xlabels.back();
        m_xlabels.pop_back();
    }
    while(!m_ylabels.empty()){
        delete m_ylabels.back();
        m_ylabels.pop_back();
    }
}

void QAEGIGrid::updateLines(){   
    QTransform trans = m_view->transform();

    QRectF viewrect = m_view->mapToScene(m_view->viewport()->rect()).boundingRect();

    if(viewrect.width()*viewrect.height()==0)
        return;

    QGraphicsLineItem* line=NULL;
    QGraphicsSimpleTextItem* txt=NULL;

    QTransform txttrans;
    txttrans.scale(1.0/trans.m11(), 1.0/trans.m22());

    // Vertical lines

    // TODO m_use_pifraction
//    lstep = M_PI;
//    m=1;
//    while(int(viewrect.height()/lstep)<3){
//        lstep /= 2;
//        m++;
//    }

    // Adapt the lines absissa to the viewport
    double f = std::log10(float(viewrect.width()));
    int fi;
    if(f<0) fi=int(f-1);
    else fi = int(f);
    double lstep = pow(10.0, fi);
//    COUTD << "f=" << " fi=" << fi << " lstep=" << lstep << std::endl;
    while(int(viewrect.width()/lstep)<5) // TODO Simplify this
        lstep /= 2;
//        std::cout << "lstep=" << lstep << std::endl;

    // Update the vertical lines
    if(m_highcontrast){
        int n=0;
        for(double l=int(viewrect.left()/lstep)*lstep; l<=viewrect.right(); l+=lstep){
            if(n>=int(m_vertlinesback.size())){
                line = new QGraphicsLineItem(this);
                line->setPen(m_pen_hc_back);
                m_vertlinesback.push_back(line);
            }
            m_vertlinesback[n]->setLine(QLineF(l+1.0/trans.m11(), viewrect.top(), l+1.0/trans.m11(), viewrect.bottom()));
            m_vertlinesback[n]->show();
            n++;
        }
        while(n<int(m_vertlinesback.size())){
            m_vertlinesback[n]->hide();
            n++;
        }
    }
    int n=0;
    for(double l=int(viewrect.left()/lstep)*lstep; l<=viewrect.right(); l+=lstep){
        if(n>=int(m_vertlines.size())){
            line = new QGraphicsLineItem(this);
            if(m_highcontrast)  line->setPen(m_pen_hc_front);
            else                line->setPen(m_pen_lines);
            m_vertlines.push_back(line);
        }
        m_vertlines[n]->setLine(QLineF(l, viewrect.top(), l, viewrect.bottom()));
        m_vertlines[n]->show();
        n++;
    }
    while(n<int(m_vertlines.size())){
        m_vertlines[n]->hide();
        n++;
    }

    // Write the absissa of the vertical lines
    if(m_highcontrast){
        int n=0;
        for(double l=int(viewrect.left()/lstep)*lstep; l<=viewrect.right(); l+=lstep){
            if(n<int(m_xlabelsback.size())){
                txt = m_xlabelsback[n];
            }
            else{
                txt = new QGraphicsSimpleTextItem(this);
                txt->setFont(m_font_hc_back);
                txt->setZValue(0.9);
                if(m_highcontrast)  txt->setBrush(m_pen_hc_back.color());
                else                txt->setBrush(m_pen_labels.color());
                m_xlabelsback.push_back(txt);
            }
            QString str = QString("%1"+m_xunit).arg(l);
            txt->setText(str);
            txt->setTransform(txttrans);
            txt->setPos(l+1.0/trans.m11(), viewrect.bottom()-(m_qfm_labels.height()-2)/trans.m22()+1.0/trans.m22());
            m_xlabelsback[n]->show();
            n++;
        }
        while(n<int(m_xlabels.size())){
            m_xlabelsback[n]->hide();
            n++;
        }
    }
    n=0;
    for(double l=int(viewrect.left()/lstep)*lstep; l<=viewrect.right(); l+=lstep){
        if(n<int(m_xlabels.size())){
            txt = m_xlabels[n];
        }
        else{
            txt = new QGraphicsSimpleTextItem(this);
            txt->setFont(m_font);
            txt->setZValue(1.0);
            if(m_highcontrast)  txt->setBrush(m_pen_hc_front.color());
            else                txt->setBrush(m_pen_labels.color());
            m_xlabels.push_back(txt);
        }
        QString str = QString("%1"+m_xunit).arg(l);
//        QString str = QTime::fromMSecsSinceStartOfDay(int(l*1000+0.5)).toString("mm:ss.zzz");
//        COUTD << l << " " << str << std::endl;
        txt->setText(str);
        txt->setTransform(txttrans);
        txt->setPos(l, viewrect.bottom()-(m_qfm_labels.height()-2)/trans.m22());
        m_xlabels[n]->show();
        n++;
    }
    while(n<int(m_xlabels.size())){
        m_xlabels[n]->hide();
        n++;
    }

    // Horizontal lines

    // Adapt the lines ordinates to the viewport
    f = std::log10(float(viewrect.height()));
    if(f<0) fi=int(f-1);
    else fi = int(f);
    lstep = pow(10.0, fi);
    while(int(viewrect.height()/lstep)<3) // TODO Simplify this
        lstep /= 2;

    // Update the horizontal lines
    if(m_highcontrast){
        int n=0;
        for(double l=int(viewrect.top()/lstep)*lstep; l<=viewrect.bottom(); l+=lstep){
            if(n>=int(m_horilinesback.size())){
                line = new QGraphicsLineItem(this);
                line->setPen(m_pen_hc_back);
                m_horilinesback.push_back(line);
            }
            m_horilinesback[n]->setLine(QLineF(viewrect.left(), l+1.0/trans.m22(), viewrect.right(), l+1.0/trans.m22()));
            m_horilinesback[n]->show();
            n++;
        }
        while(n<int(m_horilinesback.size())){
            m_horilinesback[n]->hide();
            n++;
        }
    }
    n=0;
    for(double l=int(viewrect.top()/lstep)*lstep; l<=viewrect.bottom(); l+=lstep){
        if(n>=int(m_horilines.size())){
            line = new QGraphicsLineItem(this);
            if(m_highcontrast)  line->setPen(m_pen_hc_front);
            else                line->setPen(m_pen_lines);
            m_horilines.push_back(line);
        }
        m_horilines[n]->setLine(QLineF(viewrect.left(), l, viewrect.right(), l));
        m_horilines[n]->show();
        n++;
    }
    while(n<int(m_horilines.size())){
        m_horilines[n]->hide();
        n++;
    }

    // Write the ordinates of the horizontal lines
    if(m_highcontrast){
        n=0;
        for(double l=int(viewrect.top()/lstep)*lstep; l<=viewrect.bottom(); l+=lstep){
            double l4txt = l;
            if(std::abs(l4txt)<std::numeric_limits<double>::epsilon())
                l4txt = 0.0;
            if(n<int(m_ylabelsback.size())){
                txt = m_ylabelsback[n];
            }
            else{
                txt = new QGraphicsSimpleTextItem(this);
                txt->setFont(m_font_hc_back);
                txt->setZValue(0.9);
                if(m_highcontrast)  txt->setBrush(m_pen_hc_back.color());
                else                txt->setBrush(m_pen_labels.color());
                m_ylabelsback.push_back(txt);
            }
            txt->setText(QString("%1"+m_yunit).arg(m_mathyaxis?-l4txt:l4txt));
            txt->setTransform(txttrans);
            txt->setPos(viewrect.left()+1.0/trans.m11(), l4txt-(m_qfm_labels.height()-2)/trans.m22()+1.0/trans.m22());
            m_ylabelsback[n]->show();
            n++;
        }
        while(n<int(m_ylabelsback.size())){
            m_ylabelsback[n]->hide();
            n++;
        }
    }
    n=0;
    for(double l=int(viewrect.top()/lstep)*lstep; l<=viewrect.bottom(); l+=lstep){
        double l4txt = l;
        if(std::abs(l4txt)<std::numeric_limits<double>::epsilon())
            l4txt = 0.0;
        if(n<int(m_ylabels.size())){
            txt = m_ylabels[n];
        }
        else{
            txt = new QGraphicsSimpleTextItem(this);
            txt->setFont(m_font);
            txt->setZValue(1.0);
            if(m_highcontrast)  txt->setBrush(m_pen_hc_front.color());
            else                txt->setBrush(m_pen_labels.color());
            m_ylabels.push_back(txt);
        }
        txt->setText(QString("%1"+m_yunit).arg(m_mathyaxis?-l4txt:l4txt));
        txt->setTransform(txttrans);
        txt->setPos(viewrect.left(), l4txt-(m_qfm_labels.height()-2)/trans.m22());
        m_ylabels[n]->show();
        n++;
    }
    while(n<int(m_ylabels.size())){
        m_ylabels[n]->hide();
        n++;
    }
}

void QAEGIGrid::setMathYAxis(bool mathyaxis){
    m_mathyaxis = mathyaxis;
}
