#ifndef QAEMATH_H
#define QAEMATH_H

#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>
#include <complex>

namespace qae {

#define TLTRIGT double

extern int tltrig_N;
extern TLTRIGT tltrig_v2n;
extern std::vector<TLTRIGT> tltrig_cos;
extern std::vector<std::complex<TLTRIGT> > tltrig_expi;
static TLTRIGT PI2 = 2.0*M_PI;
static TLTRIGT IPI2 = 1.0/(2.0*M_PI);
void tltrig_init(int N);

inline TLTRIGT tlcos(TLTRIGT v){
    if(tltrig_N==-1)
        tltrig_init(1024);

    if(v<0.0)
        v = -v;
    if(v>=PI2)
        v -= PI2*int(v*IPI2);

    return tltrig_cos[int(v*tltrig_v2n)];
}

inline TLTRIGT tlsin(TLTRIGT v){
    return tlcos(v-M_PI/2.0);
}

inline std::complex<TLTRIGT> tlexpi(TLTRIGT v){
    // return std::complex<TLTRIGT>(qae::tlcos(v), qae::tlsin(v));

    if(tltrig_N==-1)
        tltrig_init(1024);

    if(v<0.0)
        v += PI2*(1+int(-v*IPI2));
    else if(v>=PI2)
        v -= PI2*int(v*IPI2);

    return tltrig_expi[int(v*tltrig_v2n)];
}

};

#endif // QAEMATH_H
