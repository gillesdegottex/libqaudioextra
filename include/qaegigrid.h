/*
Copyright (C) 2015  Gilles Degottex <gilles.degottex@gmail.com>

This file is part of "libqaudioextra".

"libqaudioextra" is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"libqaudioextra" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QAEGRAPHICSITEMGRID_H
#define QAEGRAPHICSITEMGRID_H

#include <QGraphicsItemGroup>
#include <QPen>
#include <QFont>
#include <QFontMetrics>

#include <deque>

class QGraphicsView;

class QAEGIGrid : public QGraphicsItemGroup
{
    QGraphicsView* m_view;

    std::deque<QGraphicsLineItem*> m_vertlinesback;
    std::deque<QGraphicsLineItem*> m_vertlines;
    std::deque<QGraphicsSimpleTextItem*> m_xlabelsback;
    std::deque<QGraphicsSimpleTextItem*> m_xlabels;
    std::deque<QGraphicsLineItem*> m_horilinesback;
    std::deque<QGraphicsLineItem*> m_horilines;
    std::deque<QGraphicsSimpleTextItem*> m_ylabelsback;
    std::deque<QGraphicsSimpleTextItem*> m_ylabels;

    QPen m_pen_hc_back;
    QPen m_pen_hc_front;
    QPen m_pen_lines;

    QFont m_font;
    QFont m_font_hc_back;
    QFontMetrics m_qfm_labels;
    QPen m_pen_labels;
    QString m_xunit;
    QString m_yunit;
    bool m_highcontrast;
//    bool m_pifraction;
    bool m_mathyaxis;

public:
    QAEGIGrid(QGraphicsView* view, const QString& xunit, const QString& yunit);
    ~QAEGIGrid();

    void updateLines();

    void setXUnit(const QString& xunit){m_xunit=xunit;}
    void setYUnit(const QString& yunit){m_yunit=yunit;}
    void setMathYAxis(bool mathyaxis);
    void setFont(const QFont& font);
    void setHighContrast(bool highcontrast);
//    void setUsePiFraction(bool use_pifraction){m_pifraction=pifraction;}
};

#endif // QAEGRAPHICSITEMGRID_H
