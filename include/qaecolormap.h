/*
Copyright (C) 2014  Gilles Degottex <gilles.degottex@gmail.com>

This file is part of "libqaudioextra".

"libqaudioextra" is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"libqaudioextra" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _QAECOLORMAP_H_
#define _QAECOLORMAP_H_

#include <cmath>
#include <vector>
#include <algorithm>
#include <numeric>

#include <qnumeric.h>
#include <QApplication>
#include <QStringList>
#include <QColor>
#include "qaehelpers.h"

class QAEColorMap {
    static std::vector<QAEColorMap*> sm_colormaps;

public:
    QAEColorMap(){
        sm_colormaps.push_back(this);
    }
    virtual void setColor(const QColor& color){Q_UNUSED(color)}
    virtual bool isColored(){return false;}
    virtual bool isTransparent() {return false;}
    virtual QString name()=0;
    virtual QRgb map(float y)=0;
    virtual QRgb mapshort(int y)=0;
    inline QRgb operator ()(float y) {return map(y);}
    static QStringList getAvailableColorMaps() {
        QStringList list;
        for(std::vector<QAEColorMap*>::iterator it=sm_colormaps.begin(); it!=sm_colormaps.end(); ++it)
            list.append((*it)->name());
        return list;
    }
    static QAEColorMap& getAt(int index) {
        return (*sm_colormaps.at(index));
    }
};

class QAEColorMapGray : public  QAEColorMap {
public:
    virtual QString name() {return "Gray";}
    virtual QRgb map(float y){
        y = 1.0-y;

        int color = 255;
        if(!qIsInf(y))
            color = 255*y;

        if(color<0) color = 0;
        else if(color>255) color = 255;

        return qRgb(color, color, color);
    }
    virtual QRgb mapshort(int y){
        y = 65536-y;
        return qRgb(y/256, y/256, y/256);
    }
};

class QAEColorMapJet : public  QAEColorMap {
public:
    virtual QString name() {return "Jet";}
    virtual QRgb map(float y){
        y = 1.0-y;
        float red = 0;
        if(y<0.25) red = 4*y+0.5;
        else       red = -4*y+2.5;
        if(red>1.0)      red=1.0;
        else if(red<0.0) red=0.0;

        float green = 0;
        if(y<0.5)  green = 4*y-0.5;
        else       green = -4*y+3.5;
        if(green>1.0)      green=1.0;
        else if(green<0.0) green=0.0;

        float blue = 0;
        if(y<0.75) blue = 4*y-1.5;
        else       blue = -4*y+4.5;
        if(blue>1.0)      blue=1.0;
        else if(blue<0.0) blue=0.0;

        return qRgb(int(255*red), int(255*green), int(255*blue));
    }
    virtual QRgb mapshort(int y){ // y in [0, 65536[
        y = 65536-y;

        int red = 0;
        if(y<16384) red = (4*y+32768);
        else        red = (-4*y+163840);
        if(red>65536)  red=65536;
        else if(red<0) red=0;

        int green = 0;
        if(y<32768)  green = 4*y-32768;
        else         green = -4*y+229376;
        if(green>65536)  green=65536;
        else if(green<0) green=0;

        int blue = 0;
        if(y<49152) blue = 4*y-98304;
        else        blue = -4*y+294912;
        if(blue>65536)  blue=65536;
        else if(blue<0) blue=0;

        return qRgb(255*red/65536, 255*green/65536, 255*blue/65536);
    }
};

class QAEColorMapTransparent : public  QAEColorMap {
    QColor m_color;
public:
    virtual void setColor(const QColor& color){m_color=color;}
    virtual bool isColored(){return true;}
    virtual bool isTransparent() {return true;}
    virtual QString name() {return "Comparative";}
    virtual QRgb map(float y){

        int color = 255;
        if(!qIsInf(y))
            color = 255*y;

        if(color<0) color = 0;
        else if(color>255) color = 255;

//        QColor c = m_color.toHsv();
//        c.setHsv(c.hue(), c.saturation(), int((color/255.0)*c.lightness())); // Good one for dark
        QColor c = m_color;
        c.setAlphaF(color/255.0);

        return c.rgba();
    }
    virtual QRgb mapshort(int y){
        y = 65536-y;
        return qRgb(y/256, y/256, y/256);
    }
};

#endif // _QAECOLORMAP_H_
