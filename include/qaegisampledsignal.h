/*
Copyright (C) 2014  Gilles Degottex <gilles.degottex@gmail.com>

This file is part of "libqaudioextra".

"libqaudioextra" is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"libqaudioextra" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QAEGISAMPLEDSIGNAL_H
#define QAEGISAMPLEDSIGNAL_H

#include <vector>
#include <deque>

#include <QGraphicsItem>
#include <QPen>

#ifdef SIGPROC_FLOAT
#define WAVTYPE float
#else
#define WAVTYPE double
#endif

class QGraphicsView;
class QPainter;

class QAEGISampledSignal : public QGraphicsItem
{
    std::vector<double>* m_times;
    std::vector<WAVTYPE>* m_signal;
    WAVTYPE m_min;
    WAVTYPE m_max;
    double m_mean_fs;
    bool m_show_zero_values;

    // For below: shown value = gain*value + shift
    double m_gain;  // Multiplier to the amplitude of the signal values
    double m_shift; // Additive to the amplitude of the signal values

    // Display
    QGraphicsView* m_view;
    QPen m_pen;
    float m_markradius;

public:
    QAEGISampledSignal(std::vector<double>* times, std::vector<WAVTYPE>* signal, QGraphicsView* view);

    void updateMinMaxValues();

    double getMinValue() const;
    double getMaxValue() const;
    double getMaxAbsoluteValue() const;
    QPen getPen() const {return m_pen;}

    void setTimes(std::vector<double>* times);
    void setSignal(std::vector<WAVTYPE>* sig);
    void setGain(WAVTYPE gain)          {m_gain=gain; prepareGeometryChange();}
    void setShift(WAVTYPE shift)        {m_shift=shift; prepareGeometryChange();}
    void setPen(const QPen& pen)        {m_pen=pen;}
    void setShowZeroValues(bool show_zero_values){m_show_zero_values=show_zero_values;}

    virtual QRectF boundingRect() const;
    void updateGeometry() {prepareGeometryChange();}
    virtual void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0);
};

#endif // QAEGISAMPLEDSIGNAL_H
