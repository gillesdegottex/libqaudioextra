/*
Copyright (C) 2014  Gilles Degottex <gilles.degottex@gmail.com>

This file is part of "libqaudioextra".

"libqaudioextra" is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"libqaudioextra" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QAEGIUNIFORMLYSAMPLEDSIGNAL_H
#define QAEGIUNIFORMLYSAMPLEDSIGNAL_H

#include <vector>
#include <deque>

#include <QGraphicsItem>
#include <QPen>

#ifdef SIGPROC_FLOAT
#define WAVTYPE float
#else
#define WAVTYPE double
#endif

class QGraphicsView;
class QPainter;

class QAEGIUniformlySampledSignal : public QGraphicsItem
{
    std::vector<WAVTYPE>* m_seq;
    WAVTYPE m_min;
    WAVTYPE m_max;

    double m_fs;    // Sampling rate of the signal
    qint64 m_delay; // [number of sample] Delay of the first sample
    double m_gain;  // Multiplier to the amplitude of the signal values
    WAVTYPE m_clip_min;
    WAVTYPE m_clip_max;

    // Display
    QGraphicsView* m_view;
    QPen m_pen;
    float m_markradius;

    // Cache
    class PaintParameters{
    public:
        QRect m_fullpixrect;
        QRectF m_viewrect;
        int m_winpixdelay;

        std::vector<WAVTYPE>* m_seq; // The used wav to compute the DFT on.

        void clear(){
            m_fullpixrect = QRect();
            m_viewrect = QRectF();
            m_winpixdelay = 0;
            m_seq = NULL;
        }

        PaintParameters(){clear();}
        PaintParameters(const QRect& fullpixrect, const QRectF& viewrect, int winpixdelay, std::vector<WAVTYPE>* seq){
            m_fullpixrect = fullpixrect;
            m_viewrect = viewrect;
            m_winpixdelay = winpixdelay;
            m_seq = seq;
        }

        bool operator==(const PaintParameters& param) const;
        bool operator!=(const PaintParameters& param) const {return !((*this)==param);}

        inline bool isEmpty() const {return m_fullpixrect.isNull() || m_viewrect.isNull();}
    };
    PaintParameters m_paint_params;
    std::deque<WAVTYPE> m_cache_mins;    // Cache for min values
    std::deque<WAVTYPE> m_cache_maxs;    // Cache for max values

public:
    QAEGIUniformlySampledSignal(std::vector<WAVTYPE>* seq, double fs, QGraphicsView* view);

    void updateMinMaxValues();

    double gain() const {return m_gain;}
    qint64 delay() const {return m_delay;}
    double getMinValue() const;
    double getMaxValue() const;
    double getMaxAbsoluteValue() const;
    QPen getPen() const {return m_pen;}

    void setSignal(std::vector<WAVTYPE>* seq);
    void setPen(const QPen& pen)        {m_pen=pen;}
    void setSamplingRate(double fs);
    void setGain(double gain);
    void setDelay(qint64 delay);
    void setClip(double min, double max);
    void clearCache()                   {m_paint_params.clear();}

    virtual QRectF boundingRect() const;
    void updateGeometry() {prepareGeometryChange();}
    virtual void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0);
};

#endif // QAEGIUNIFORMLYSAMPLEDSIGNAL_H
